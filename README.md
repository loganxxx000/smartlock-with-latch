# SmartLock with Latch



Development of a smart lock based in Arduino MKR1000 connected with Latch (Elevenpaths). To connect with HTTPS servers of Latch I used a reverse proxy (Ningx) in Raspberry Pi Zero cause Arduino board can't handle SSL Handshake by his own. In Raspberry I created a short python script to launch Motion server when Latch is blocked to see with Picam who is calling the door by streaming, after some seconds automatically will finish Motion demon.