
/*
 Control Cerradura Inteligente con Latch
 
 creado Abril 2018
 por Iván Ferrer
 */

// **********************************  LIBRERIAS ARDUINO  *****************************************************
#include <SPI.h>
#include <WiFi101.h>
#include <WiFiUdp.h>
#include <WiFiClient.h>
//#include <WiFiSSLClient.h>
#include <TimeLib.h>                          // github.com/PaulStoffregen/Time/blob/master/TimeLib.h
//#include <Time.h>                             // playground.arduino.cc/Code/Time   Arduino Time library
#include <sha1.h>                             // github.com/Cathedrow/Cryptosuite   Cryptographic suite for Arduino (SHA, HMAC-SHA) 
#include <Base64.h>                           // github.com/adamvr/arduino-base64   Copyright (C) 2013 Adam Rudd
#include <avr/pgmspace.h>                     // AVR libreria libc para almacenamiento datos en memoria flash en lugar de SRAM #######

// **********************************  CONFIGURACIÓN WIFI  *****************************************************
#include "arduino_secrets.h" 
///////introduce los datos sensiblesen la pestaña/arduino_secrets.h
char ssid[] = SECRET_SSID;        // your network SSID (name)
char pass[] = SECRET_PASS;        // your network password (use for WPA, or use as key for WEP)
int keyIndex = 0;                 // your network key Index number (needed only for WEP)

// ********************************** DATOS SERVIDOR LATCH Y DIRECCION IP  ****************************************
int status = WL_IDLE_STATUS;
// if you don't want to use DNS (and reduce your sketch size)
// use the numeric IP instead of the name for the server:
//IPAddress server(52,30,249,113);          // dirección IP para "https://latch.elevenpaths.com" (sin DNS)
//char server[] = "latch.elevenpaths.com";    // dirección por nombre "https://latch.elevenpaths,com" (con DNS)
//char server1[] = "iot-latch.e-paths.com";   // dirección servidor pruebas
IPAddress server2(192,168,43,250);           // IP Reverse Proxy (Intermediación SSL)  

// **********************************  LIBRERIA SSL - PÁGINAS HTTPS  *****************************************
// Initialize the Ethernet client library
// with the IP address and port of the server
// that you want to connect to (port 80 is default for HTTP):
//WiFiSSLClient client;                     // servidor https
WiFiClient client;                          // servidor http (Reverse Proxy)

// ********************************** SERVIDOR NTP - DATOS  *************************************************
unsigned int localPort = 2390;              // puerto local de escucha de paquetes UDP
IPAddress timeServer(130, 206, 3, 166);     // RedIRIS NTP Server
const int NTP_PACKET_SIZE = 48;             // NTP time stamp (sello temporal NTP) esta en los primeros 48 bytes del mensaje
byte packetBuffer[ NTP_PACKET_SIZE];        //buffer para mantener llegada y salida de paquetes
time_t horaAnterior = 0;

// Necesario para enviar paquetes UDP al servidor NTP
// Instancia UDP para enviar y recibir paquetes UDP
WiFiUDP Udp;

// ************************************************  VARIABLES API LATCH  *************************************************
char  Date[] = "X-11Paths-Date:";
char Auth[] = "Authorization:11PATHS";
const char applicationId[] = "XXXXxeB9KHpJxpBCXXXX";
const uint8_t Secret[] = "XXXXXJVRJrQB3F84z4ZVJ3tctE73aHmd6DwXXXXX";

// Variables de apoyo
String X11PathsDate;
String Cabecera1, Cabecera2, URL;
boolean latchStatus;

#define STR_DATE_SIZE             20
#define MAIN_BUFF_SIZE            128
#define LATCH_HMAC_SIZE           20
#define LATCH_HMAC_STR_SIZE       32
#define LATCH_SECRET_SIZE         40

// Long strings to Flash Memory 
const char     LatchAppID[]       PROGMEM {"XXXXxeB9KHpJxpBCXXXX"};
const uint8_t  LatchSecret[]      PROGMEM {"XXXXXJVRJrQB3F84z4ZVJ3tctE73aHmd6DwXXXXX"}; // uint8_t array mandatory, without \0 at end.
const char     LatchAccountID[]   PROGMEM {"XXXXX6u6vT3paRjcBu8zvTMEF7JQyR4QFx296uFj8gt2ButsFRjaQNGC9ZiXXXXX"};

// Aux strings
const char     LatchAPI[]         PROGMEM {"/api/1.3/status/"};     // servidor https
//const char     LatchAPI[]         PROGMEM {"/api/1.0/status/"};   // servidor pruebas
const char     LatchAuth[]        PROGMEM {"Authorization:11PATHS "};
const char     LatchDate[]        PROGMEM {"X-11Paths-Date"};
const char     LatchON[]          PROGMEM {"{\"status\":\"on\"}"};

// Global buffers
char MainBuffer[MAIN_BUFF_SIZE];              // Main buffer
char DateTime[STR_DATE_SIZE];                 // Date Time Buffer 11Paths-Date format
uint8_t hmac[LATCH_HMAC_SIZE];                // HMAC buffer format uint8_t (20 bytes)
char hmac_str[LATCH_HMAC_STR_SIZE];           // HMAC buffer format string (32 bytes)
uint8_t* hmac_p = hmac;                       // Pointer to HMAC buffer format uint8_t

// *************************************** BOTÓN Y LEDS ***************************************************
const int rojo = 0;
const int amarillo = 1;
const int pulsador = 2;
int boton = 0;
const int rele = 3;

//  *******************************************  IMPRIME DATOS WIFI   ****************************************
void printWiFiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

// ************************************** ENVIO PETICIÓN NTP AL SERVIDOR DE TIEMPO  ****************************
unsigned long sendNTPpacket(IPAddress& address)
{  
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)  
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;  

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  Udp.beginPacket(address, 123); //NTP requests are to port 123  
  Udp.write(packetBuffer, NTP_PACKET_SIZE);  
  Udp.endPacket(); 
}

// *********************************************************** PETICION SERVIDOR NTP *********************************************************
void peticionNTP(){
  Serial.println("\nIniciando conexion al servidor NTP...");
  Udp.begin(localPort);

  sendNTPpacket(timeServer); // send an NTP packet to a time server
  // wait to see if a reply is available
  delay(1000);
  if ( Udp.parsePacket() ) {
    Serial.println("packet received");
    // We've received a packet, read the data from it
    Udp.read(packetBuffer, NTP_PACKET_SIZE);                  // lectura del paquete en el buffer

    //the timestamp starts at byte 40 of the received packet and is four bytes,
    // or two words, long. First, extract the two words:

    unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
    unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
    // combine the four bytes (two words) into a long integer
    // this is NTP time (seconds since Jan 1 1900):
    unsigned long secsSince1900 = highWord << 16 | lowWord;
        
    // Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
    const unsigned long seventyYears = 2208988800UL;
    // subtract seventy years:
    unsigned long epoch = secsSince1900 - seventyYears;
    
    setTime(epoch);             // AÑADIMOS SEGUNDOS CALCULADOS AL SISTEMA ARDUINO
                                // YA TENEMOS HORA UTC PARA PETICIONES  
  }      
} //peticionNTP()

//  ********************************************** MOSTRAR FECHA SEGÚN FORMATO  *************************************
void muestraFecha(){
  String mes, dia, hora, minutos, segundos; 
    
  mes = compruebaFormato(month());
  dia = compruebaFormato(day());
  hora = compruebaFormato(hour());
  minutos = compruebaFormato(minute());
  segundos = compruebaFormato(second());
  X11PathsDate = String(year()) + "-" + mes + "-" + dia + " " + hora + ":" + minutos + ":" + segundos;  
  Serial.println (X11PathsDate); 
}

//  ******************************************* COMPROBACIÓN FORMATO DIGITOS HORA ********************************
String compruebaFormato (int i) {
  if (i<10){
    return ("0"+String(i));
  }
  else
    return (String(i));
}

//  ******************************************* GENERACIÓN DE LAS CABECERAS ***************************************
void generarCabeceras(){
  
  // Conversión String 'X11PathsDate' to Char 'DateTime'
  strncpy(DateTime, X11PathsDate.c_str(),20);  
    
  // Inicio HMAC-SHA1 con el Secreto Latch
  memset(MainBuffer, 0, sizeof(char)*MAIN_BUFF_SIZE);           // Clean main buffer
  memcpy_P((uint8_t*)MainBuffer,LatchSecret,LATCH_SECRET_SIZE); // Copy Latch Secret (40 bytes uint8_t) like "I8QsZC1U-----------yZeb7rMmzHml"   
  Sha1.initHmac((uint8_t*)MainBuffer,LATCH_SECRET_SIZE);        // Init SHA-1 HMAC

  // string a encriptar
  memset(MainBuffer, 0, sizeof(char)*MAIN_BUFF_SIZE);           // Vacio Main buffer
  sprintf(MainBuffer,"GET\n%s\n\n",DateTime);                   // Añado DateTime               "GET\n2015-08-27 21:55:49\n\n"
  strcat_P(MainBuffer,LatchAPI);                                // Añado PROGMEM LatchAPI       "/api/1.3/status/"
  strcat_P(MainBuffer,LatchAccountID);                          // Añado PROGMEM LatchAccountID "2294544baf39--------------------15f7b66467b49d43297"   
                       
  // Firma HMAC-SHA
  Sha1.print(MainBuffer);                                       // Calculo HMAC  
  memset(hmac,0,(LATCH_HMAC_SIZE*sizeof(uint8_t)));             // Vacio HMAC unit8_8 dst buffer
  hmac_p=Sha1.resultHmac();                                     // Calculo HMAC (20 bytes uint8_t )    
  
  // Codificación Base64
  memset(hmac_str,0,(LATCH_HMAC_STR_SIZE*sizeof(char)));        // Vacio dst string HMAC buffer
  Base64.encode(hmac_str,(char*)hmac_p, LATCH_HMAC_SIZE);       // Cofifica base64 HMAC a string HMAC buffer

  Serial.print(F("Base64: Latch Request Signature -->  "));
  Serial.println(hmac_str);

  // Cabeceras "Authorization" and "11Paths-Date" (se necesita AppID and HMAC string)
  memset(MainBuffer, 0, sizeof(char)*MAIN_BUFF_SIZE);           // Vacio Main buffer        
  strcpy_P(MainBuffer,LatchAuth);                               // Añado PROGMEM LatchAuth  {"Authorization: 11PATHS "}
  strcat_P(MainBuffer,LatchAppID);                              // Añado PROGMEM LatchAppID {"..."}
  strcat  (MainBuffer," ");                                       
  strcat  (MainBuffer,hmac_str);                                // HMAC str 32 bytes like "bVh7NwAOdlo32ZPQY6ixviNbX50="      
  strcat  (MainBuffer,"\r\n"); 
  Cabecera1 = MainBuffer;                                       // *** Copio a cabecera1 *** 
  
  memset(MainBuffer, 0, sizeof(char)*MAIN_BUFF_SIZE);           // Vacio Main buffer
  strcat_P(MainBuffer,LatchDate);                               // Añado PROGMEM LatchDate {"X-11Paths-Date"}
  strcat  (MainBuffer,": "); 
  strcat  (MainBuffer,DateTime);                                // Añado 11Paths-Date str 20 bytes like "2015-08-27 21:55:49"         
  strcat  (MainBuffer,"\r\n");
  Cabecera2 = MainBuffer;                                       // *** Copio a cabecera2 *** 

  // Genero el String URL
  memset(MainBuffer, 0, sizeof(char)*MAIN_BUFF_SIZE);           // Vacio Main buffer
  strcat_P(MainBuffer,LatchAPI);                                // Añado PROGMEM LatchAPI       "/api/1.3/status/"    
  strcat_P(MainBuffer,LatchAccountID);                          // Añado PROGMEM LatchAccountID "XXXXX44baf39--------------------15f7b66467b49dXXXXX"
  URL = MainBuffer;
      
}//generarCabeceras()

// ****************************************************** CONEXIÓN SERVIDOR LATCH ****************************************************
void conectar_servidor(){
 // Test comprobación de que se envía
 Serial.println ("Impresión de cabeceras --> ");
 Serial.println ("Cabecera1 --> "+Cabecera1);
 Serial.println ("Cabecera2 --> "+Cabecera2);
 Serial.println ("URL --> "+URL);


  Serial.println("\n Estableciendo conexión al servidor...");
  // if you get a connection, report back via serial:
  //if (client.connectSSL(server, 443)) {                                         // Servidor LATCH(https)      
  //if (client.connect(server1, 80)) {                                            // Servidor test (NO https)
  if (client.connect(server2, 8080)) {                                            // Conexión a traves SSL Intermediación
    Serial.println("conectado al servidor");
    // Make a HTTP request:
    client.print("GET ");
    client.print(URL);
    client.println(" HTTP/1.1");
    client.print("Host: ");
    client.println("latch.elevenpaths.com");
    client.println("User-Agent: Arduino/1.0"); 
    client.print(Cabecera1);    
    client.print(Cabecera2);  
    client.println("Connection: close");    
    //client.println("Connection: keep-alive");    
    client.println();

  }
  else
    Serial.println("No conectado al servidor");

  unsigned long timeout = millis();
  while (client.available() == 0) {
    if (millis() - timeout > 5000) {
      Serial.println(">>> Client Timeout !");
      client.stop();
    }
  }
  
}// conectar_servidor()


// *********************************************************** RECEPCIÓN RESPUESTA **********************************************************
String respuesta_servidor_analizada(){  

  String latch;

  // Si hay llegada de bytes disponibles desde el servidor, los lee e imprime por pantalla
  while (client.available()) {
        
    // Opción 2     CAPTURA POR STRING
    String line = client.readStringUntil('\n');
    Serial.println(line);
    char charBuf[100];    
    line.toCharArray(charBuf,100);
  
    if (strstr(charBuf,"status")){
      if (strstr(charBuf,"off")){
        latch = "off";      
      }
      else
        latch = "on";
    }  
  }

  // if the server's disconnected, stop the client:
  if (!client.connected()) {
    Serial.println();
    Serial.println("disconnecting from server.");
    client.stop();
  }
  
  return (latch);  
}// respuesta_servidor_analizada()


// ******************************************* MOVIMIENTO CERROJO Y LEDs ******************************************
void mov_cerrojo(String accion){
  int pos = 0;
  
  if (accion == "on"){
    Serial.println("Abriendo cerrojo...");
    digitalWrite (amarillo, HIGH);
    digitalWrite (rele, LOW);                                                  
    delay(8000);                // Actuación sobre el relé durante 8seg 
    Serial.println("Cerrando cerrojo...");
    digitalWrite (amarillo, LOW);
    digitalWrite (rele, HIGH);         
  }
  else {
    Serial.println("No tiene permitido abrir el cerrojo...");
    digitalWrite (rojo, HIGH);
    delay(3000);
    digitalWrite (rojo, HIGH); 
    digitalWrite (rojo, LOW);   
  }
}// mov_cerrojo()
  
//  ***********************************************************  SETUP ***********************************************
void setup() {
  //Initialize serial and wait for port to open:
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // Configuración de los pines IN/OUT 
  pinMode(amarillo, OUTPUT);                // LED amarillo
  pinMode(rojo, OUTPUT);                    // LED rojo  
  pinMode(pulsador, INPUT);                 // Entrada del pulsador
  pinMode(rele, OUTPUT);                    // Salida hacia Relé  
  digitalWrite (rele, HIGH);                // Deja cerradura cerrada

  // check for the presence of the shield:
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present");
    // don't continue:
    while (true);
  }

  // attempt to connect to WiFi network:
  while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
    status = WiFi.begin(ssid, pass);

    // wait 10 seconds for connection:
    delay(10000);
  }
  Serial.println("Connected to wifi");
  printWiFiStatus();   
  
}

void loop() {  
  boton = digitalRead(pulsador);              // Lectura del pulsador para efectuar petición de estado a Latch

  if (boton == HIGH){
    Serial.println("BOTON PULSADO");
    Serial.println("Enviando consulta...");  
    
    // Peticion Servidor NTP
    peticionNTP();
  
    muestraFecha();
    delay(500);
  
    Serial.println("\nGenerando cabeceras petición");
    generarCabeceras();
  
    // Conexión al servidor 
    Serial.println("\nConectando al servidor...");
    conectar_servidor();
  
    // Recepción Respuesta
    Serial.println("\nRecibiendo respuesta del servidor...");    
    String estado = respuesta_servidor_analizada();                    // Análisos del JSON recibido
    Serial.println ("Estado de latch: " + estado);
    mov_cerrojo(estado);                                               // Análisis de actuación a realizar     
  }

  Serial.println("...");
  delay(300);
}
