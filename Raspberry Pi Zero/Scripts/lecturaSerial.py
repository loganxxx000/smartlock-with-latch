#!/usr/bin/python
# _*_ coding: utf-8 _*_

# Importamos librerias
import serial
import shlex, subprocess
import time

# Abrimos el puerto del arduino a 9600
PuertoSerie = serial.Serial('/dev/ttyACM0', 9600)
# Creamos un buble sin fin
while True:
	# leemos hasta que encontarmos el final de linea
	line = PuertoSerie.readline()
 	# Mostramos el valor leido y eliminamos el salto de linea del final
	print line.rstrip('\n')
	# Detección de "Estado bloquedo en respuesta de Latch" para arrancar Picamera
	if 'No tiene permitido abrir el cerrojo...' in line:
		print '¡DETECTADA PULSACIÓN!'
		#command_line='./home/pi/Scripts/llamadaMotion.sh'
		#command_line2='sudo pkill -9 motion'
		#args=shlex.split(command_line)
		#args2=shlex.split(command_line2)
		#subprocess.call(args)
		#subprocess.call('/home/pi/Scripts/llamadaMotion.sh', shell=False)
		subprocess.check_call('sudo motion', shell=True)
		time.sleep(70)
		subprocess.check_call('sudo pkill -9 motion', shell=True);
		#subprocess.call('sudo pkill -9 motion', shell=False);
		#subprocess.call(args2)



